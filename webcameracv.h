﻿#ifndef WEBCAMERACV_H
#define WEBCAMERACV_H
#include <QWidget>

class CvCapture;
typedef struct _IplImage IplImage;

class WebCameraCV : public QWidget
{
    Q_OBJECT

enum ObjectType { NoObject, Aim };

public:
    WebCameraCV(QSize aimSize = QSize(300, 400), QWidget *parent = 0);
    ~WebCameraCV();
    CvCapture *cvCaptureTheCam;
    QImage qImage;
    QSize sizeHint () const;
    void drawFromCam();
    void drawAim();
    void setAimPos(const QPointF &pos);
    QImage takeFoto();
    bool cameraIsConnected();

private:
    int animationTimer;
    ObjectType current_object;
    QPointF foto_aim_centre;
    QPointF point_offset;
    QSizeF aim_size;

public slots:
    void needFoto();

signals:
    void getFoto(const QImage &img);

protected:
    void paintEvent(QPaintEvent *event);
    void timerEvent(QTimerEvent *);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *);
    void mouseDoubleClickEvent(QMouseEvent *);
};

#endif
