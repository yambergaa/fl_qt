﻿#include <QtGui>
#include "webcameracv.h"
//#include "opencv/cv.h"
#include "opencv/highgui.h"

using namespace cv;
const int animationInterval = 15; // обновлять каждые 16 ms = 62.5 FPS

WebCameraCV::WebCameraCV(QSize aimSize, QWidget *parent) : QWidget(parent)
{
    cvCaptureTheCam = cvCreateCameraCapture(0);

    setAttribute (Qt::WA_StaticContents);
    setSizePolicy (QSizePolicy::Minimum, QSizePolicy::Minimum); //минимальный размер виджета для менеджера компоновки

    qImage = QImage(size(), QImage::Format_RGB32);

    foto_aim_centre = QPointF(qImage.width()/2, qImage.height()/2);
    aim_size = aimSize;
    current_object = NoObject;

    animationTimer = startTimer(animationInterval);
};

QSize WebCameraCV::sizeHint () const
{
    return qImage.size();
}

WebCameraCV::~WebCameraCV()
{
    cvReleaseCapture(&cvCaptureTheCam); //работает когда родителя явно удаляешь
}

// Эта функция копирует IplImage 8бит, 3 канала в Qt QImage
// не выделяет новый размер для входного QImage
// Для неё нужно создать входной QImage нужного размера
int IplCopyToQImage(IplImage *img, QImage &qimg)
{
    if (!img) return 1;
    int minw = qMin(img->width, qimg.width());
    int minh = qMin(img->height, qimg.height());
    if (minw == 0 || minh == 0) return 2;

    for (int y = 0;y < minh;y++)
    {
        uchar *ptr = (uchar*)(img->imageData + y*img->widthStep);
        QRgb *ptrDst = (QRgb*)(qimg.scanLine(y));

        for (int x = 0;x<minw;x++)
        {
            int b = ptr[3*x];
            int g = ptr[3*x+1];
            int r = ptr[3*x+2];
            *ptrDst=qRgb(r,g,b);// opt:: using scanline here
            ptrDst++;
        };
    };
    return 0;
}

void WebCameraCV::timerEvent(QTimerEvent *)
{
    update();
}

void WebCameraCV::drawFromCam()
{
    IplImage *cvCamFrame = cvQueryFrame(cvCaptureTheCam); // Получаем кадр с камеры
    IplCopyToQImage(cvCamFrame, qImage); //преобразуем IplImage в QImage
    QPainter painter(this);
    painter.drawImage (0, 0, qImage);
}

QRectF rectangle_around(const QPointF &p, const QSizeF &size) // "переносит" TopLeft в центр
{
    QRectF rect(p, size);
    rect.translate(-size.width()/2, -size.height()/2);
    return rect;
}

QImage WebCameraCV::takeFoto()
{
    QRectF rF = rectangle_around(foto_aim_centre, aim_size);
    QRect r;
    r.setX ((int)rF.x());
    r.setY ((int)rF.y());
    r.setWidth ((int)rF.width());
    r.setHeight ((int)rF.height());
    return qImage.copy(r);
}

void WebCameraCV::needFoto ()
{
    emit getFoto(takeFoto());  //хахах))) названия)
}

bool WebCameraCV::cameraIsConnected()
{
    if (!cvCaptureTheCam == NULL) {
        return true;
    }else{
        return false;
    }
}

void WebCameraCV::mousePressEvent(QMouseEvent *event)
{
    QRectF r = rectangle_around(foto_aim_centre, aim_size);
    if (r.contains(event->pos())) {
        current_object = Aim;
        point_offset = r.center() - event->pos();
    } else {
        current_object = NoObject;
    }
}

void WebCameraCV::mouseMoveEvent(QMouseEvent *event)
{
    if (current_object == Aim) setAimPos(event->pos() + point_offset);
}

void WebCameraCV::setAimPos(const QPointF &pos)
{
    //const QRect oldRect = rectangle_around(foto_aim_centre, aim_size).toAlignedRect();
    foto_aim_centre = pos;
    //const QRect newRect = rectangle_around(foto_aim_centre, aim_size).toAlignedRect();
    update();//(oldRect | newRect); // это вот если нужно обновлять только конкретную область
}

void WebCameraCV::mouseReleaseEvent (QMouseEvent *)
{
    current_object = NoObject;
}

void WebCameraCV::mouseDoubleClickEvent (QMouseEvent *)
{
    emit getFoto(takeFoto());
}

void WebCameraCV::drawAim()
{
    QPainter painter(this);
    painter.setPen (QPen(Qt::green, 1, Qt::SolidLine, Qt::RoundCap));
    painter.drawRect (rectangle_around(foto_aim_centre, aim_size));
    painter.drawLine (foto_aim_centre.x()-15, foto_aim_centre.y(), foto_aim_centre.x()+15, foto_aim_centre.y());
    painter.drawLine (foto_aim_centre.x(), foto_aim_centre.y()-15, foto_aim_centre.x(), foto_aim_centre.y()+15);
}

void WebCameraCV::paintEvent (QPaintEvent *)
{
    drawFromCam ();
    drawAim();
}
