﻿#include "mainwindow.h"
#include "webcameracv.h"
#include "loadtempimage.h"
//#include <iostream>
#include <QtGui>

QString boolToStr(bool a2)
{
    if (a2 == true) {
        return "True";
    }else
    {
        return "False";
    }
}

QSize argToSize(QString arg)
{
    QStringList list = arg.split ("x");
    return QSize(list[0].toInt(), list[1].toInt());
}

MainWindow::MainWindow(char *argv[], QWidget *parent) : QWidget(parent)
{
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF8"));

    setWindowTitle(tr("WebCameraFoto"));

    WebCameraCV *cameraView = new WebCameraCV(argv[2] != NULL ? argToSize(argv[2]) : argToSize("300x400"));

    if (!cameraView->cameraIsConnected()) {
        showErrorDialog();
        cameraView->hide();
    }else{
        QGroupBox *leftGroup = new QGroupBox;
        leftGroup->setTitle("");
        QGroupBox *fotoGroup = new QGroupBox;
        fotoGroup->setTitle("");

        LoadTempImage *loadTempImage = new LoadTempImage(argv[2] != NULL ? argToSize(argv[2]) : argToSize("300x400"));

        if (argv[1] != NULL){
            loadTempImage->setSavePath(argv[1]);
        }

        QHBoxLayout *mainLayout = new QHBoxLayout;
        QVBoxLayout *fotoLayout = new QVBoxLayout(fotoGroup);
        QVBoxLayout *leftLayout = new QVBoxLayout(leftGroup);
        QPushButton *btTake = new QPushButton(tr("Сделать снимок"));
        btSave = new QPushButton(tr("Ok"));
        btSave->setEnabled(false);
        QPushButton *btExit = new QPushButton(tr("Отмена"));


        fotoLayout->addWidget(cameraView);
        fotoLayout->addStretch();

        leftLayout->addWidget(loadTempImage);
        leftLayout->addWidget(btTake);
        leftLayout->addWidget(btSave);
        leftLayout->addWidget(btExit);
        leftLayout->addStretch();

        mainLayout->addWidget(fotoGroup);
        mainLayout->addWidget(leftGroup);
        mainLayout->addStretch();

        connect (btTake, SIGNAL(clicked()), cameraView, SLOT(needFoto()));
        connect (cameraView, SIGNAL(getFoto(QImage)), this, SLOT(enableButton()));
        connect (cameraView, SIGNAL(getFoto(QImage)), loadTempImage, SLOT(loadImage(QImage)));
        connect (btSave, SIGNAL(clicked()), loadTempImage, SLOT(saveImage()));
        connect (btSave, SIGNAL(clicked()), this, SLOT(close()));
        connect (btExit, SIGNAL(clicked()), this, SLOT(close()));
        setLayout (mainLayout);
    }
    setFixedSize(sizeHint());
}

void MainWindow::enableButton()
{
    btSave->setEnabled(true);
}

void MainWindow::showErrorDialog ()
{
    QLabel *label1 = new QLabel(tr("Устройство захвата изображения не найдено!"));
    QPushButton *pb1 = new QPushButton(tr("Выход"));
    connect (pb1, SIGNAL(clicked()), this, SLOT(close()));
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(label1);
    mainLayout->addWidget(pb1);
    mainLayout->addStretch();
    setLayout (mainLayout);
}
