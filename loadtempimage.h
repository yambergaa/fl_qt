﻿#ifndef LOADTEMPIMAGE_H
#define LOADTEMPIMAGE_H

#include <QWidget>

class LoadTempImage : public QWidget
{
    Q_OBJECT

public:
    explicit LoadTempImage(QSize size = QSize(300,400), QWidget *parent = 0);
    ~LoadTempImage();
    QSize sizeHint () const;
    QImage loadedImage;
    void drawFoto();
    void setSavePath(QString path);
    QString savePath;

public slots:
    void loadImage(QImage img);
    void saveImage();

protected:
    void paintEvent(QPaintEvent *);
};

#endif
