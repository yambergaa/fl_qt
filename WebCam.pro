#QMAKE_LIBDIR += "C:/Qt/4.8.4/plugins/imageformats/"
#"C:/OpenCV/build/x86/mingw/lib" \


QT += gui

TARGET =
TEMPLATE = app
#QTPLUGIN += qjpeg

CONFIG += static

INCLUDEPATH +=  . \
                "C:/OpenCV/build/include"
                #"C:\\Qt\\4.8.4\\plugins"

win32:LIBS += "C:/OpenCV/build/x86/mingw/lib/libopencv_highgui245.dll.a"
              #"C:/Qt/4.8.4/plugins/imageformats/libqjpeg4.a"

unix:LIBS += -lcv -lhighgui

HEADERS  += mainwindow.h \
    webcameracv.h \
    loadtempimage.h

SOURCES += main.cpp mainwindow.cpp \
    webcameracv.cpp \
    loadtempimage.cpp
