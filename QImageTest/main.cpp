﻿#include <iostream>
#include <QtGui/QImage>
#include <QtGui/QImageWriter>
#include <QCoreApplication>

//#include <QtPlugin>

//Q_IMPORT_PLUGIN(qjpeg)

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    cout << "Supported Image Formats:" << endl;

    for (int i = 0; i < QImageWriter::supportedImageFormats().size(); ++i) {
        cout << QImageWriter::supportedImageFormats().at(i).constData() << endl;
    }

    uchar *buffer = (uchar *)malloc(32 * 32 * 3);

    QImage image = QImage (buffer, 32, 32, 32 * 3, QImage::Format_RGB32);
    QString str = "test.bmp";

    bool saved = image.save(str);

    cout << "Saved image " << saved << endl;

    
    return a.exec();
}
