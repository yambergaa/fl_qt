QT      += core gui

#QTPLUGIN += qjpeg

TARGET = QImageTest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app
DEPENDPATH += .
INCLUDEPATH += .

win32:LIBS += "C:/Qt/4.8.4/plugins/imageformats/libqjpeg4.a"

SOURCES += main.cpp
