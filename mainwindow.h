﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QPushButton>
#include <QPainter>

class WebCameraCV;

class MainWindow : public QWidget
{
    Q_OBJECT
    
public:
    explicit MainWindow(char *argv[], QWidget *parent = 0);
    void showErrorDialog();

    QPushButton *btSave;

public slots:
    void enableButton();

};

#endif
