﻿#include "mainwindow.h"
#include "webcameracv.h"
#include "loadtempimage.h"
//#include <QDebug>
#include <iostream>
#include <QtGui>

QString boolToStr(bool a2)
{
    if (a2 == true) {
        return "True";
    }else
    {
        return "False";
    }
}

MainWindow::MainWindow(char *argv[], QWidget *parent) : QWidget(parent)
{
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF8"));

    setWindowTitle(tr("WebCameraFoto"));

    //qDebug() << tr("Test") << argv[1];

    WebCameraCV *cameraView = new WebCameraCV;
    if (!cameraView->cameraIsConnected()) {
        showErrorDialog();
        cameraView->hide();
    }else{
        QGroupBox *leftGroup = new QGroupBox;
        leftGroup->setTitle("");
        QGroupBox *fotoGroup = new QGroupBox;
        fotoGroup->setTitle("");

        //LoadTempImage *loadTempImage = new LoadTempImage;
        loadTempImage = new LoadTempImage;

        if (argv[1] != NULL){
            loadTempImage->setSavePath(argv[1]);

            QString s = "";
            s.append ("|");
            s.append ("argv[0]=");
            s.append (argv[0]);
            s.append ("|");
            s.append ("argv[1]=");
            s.append (argv[1]);
            s.append ("|");
            s.append ("argv[2]=");
            s.append (argv[2]);
            s.append ("|");
            //s.append ("argv[2]=");
            //s.append (argv[2]);
            //s.append ("|");
            setWindowTitle(s);
        }

        QHBoxLayout *mainLayout = new QHBoxLayout;
        QVBoxLayout *fotoLayout = new QVBoxLayout(fotoGroup);
        QVBoxLayout *leftLayout = new QVBoxLayout(leftGroup);
        QPushButton *btTake = new QPushButton(tr("Сделать снимок"));
        btSave = new QPushButton(tr("Ok"));
        btSave->setEnabled(false);
        QPushButton *btExit = new QPushButton(tr("Отмена"));


        fotoLayout->addWidget(cameraView);
        fotoLayout->addStretch();

        leftLayout->addWidget(loadTempImage);
        leftLayout->addWidget(btTake);
        leftLayout->addWidget(btSave);
        leftLayout->addWidget(btExit);
        leftLayout->addStretch();

        mainLayout->addWidget(fotoGroup);
        mainLayout->addWidget(leftGroup);
        mainLayout->addStretch();

        connect (btTake, SIGNAL(clicked()), cameraView, SLOT(needFoto()));
        connect (cameraView, SIGNAL(getFoto(QImage)), this, SLOT(enableButton()));
        connect (cameraView, SIGNAL(getFoto(QImage)), loadTempImage, SLOT(loadImage(QImage)));
        connect (btSave, SIGNAL(clicked()), loadTempImage, SLOT(saveImage()));

        connect (btSave, SIGNAL(clicked()), this, SLOT(someDebug()));  //deb
        //connect (btSave, SIGNAL(clicked()), this, SLOT(close()));  //no deb

        connect (btExit, SIGNAL(clicked()), this, SLOT(close()));
        setLayout (mainLayout);
    }
    setFixedSize(sizeHint());
}

void MainWindow::someDebug (){
    QString s1 = windowTitle ();
    s1.append (boolToStr(loadTempImage->a1));

    setWindowTitle(s1);
}

void MainWindow::enableButton()
{
    btSave->setEnabled(true);
}

void MainWindow::showErrorDialog ()
{
    QLabel *label1 = new QLabel(tr("Устройство захвата изображения не найдено!"));
    QPushButton *pb1 = new QPushButton(tr("Выход"));
    connect (pb1, SIGNAL(clicked()), this, SLOT(close()));
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(label1);
    mainLayout->addWidget(pb1);
    mainLayout->addStretch();
    setLayout (mainLayout);
}
