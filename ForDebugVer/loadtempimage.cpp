﻿#include "loadtempimage.h"
#include <QtGui>

LoadTempImage::LoadTempImage(QWidget *parent) : QWidget(parent)
{
    loadedImage = QImage(300, 400, QImage::Format_RGB32);
    savePath = "temp.jpg";
    a1 = false;
}

LoadTempImage::~LoadTempImage ()
{
}

QSize LoadTempImage::sizeHint() const
{
    return loadedImage.size();
}

void LoadTempImage::loadImage(QImage img)
{
    loadedImage = img;
    update ();
    //updateGeometry (); //лишнее, не?
}

void LoadTempImage::saveImage()
{
    a1 = loadedImage.save(savePath);
}

void LoadTempImage::setSavePath(QString path)
{
    savePath = path;
}

void LoadTempImage::drawFoto()
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.setPen (QPen(Qt::black, 1, Qt::SolidLine, Qt::RoundCap));
    painter.drawRect (0, 0, 300-1, 400-1);
    painter.drawImage(0, 0, loadedImage);
}

void LoadTempImage::paintEvent(QPaintEvent *)
{
    drawFoto();
}

