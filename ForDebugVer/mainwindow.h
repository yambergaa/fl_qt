﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QPushButton>
#include <QPainter>
class LoadTempImage; //deb

class WebCameraCV;

class MainWindow : public QWidget
{
    Q_OBJECT
    
public:
    explicit MainWindow(char *argv[], QWidget *parent = 0);
    void showErrorDialog();

    QPushButton *btSave;
    LoadTempImage *loadTempImage; //deb

public slots:
    void enableButton();
    void someDebug();

};

#endif
