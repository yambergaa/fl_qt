TARGET =
TEMPLATE = app

INCLUDEPATH += "C:/OpenCV/build/include"

win32:LIBS += "C:/OpenCV/build/x86/mingw/lib/libopencv_highgui245.dll.a"
unix:LIBS += -lcv -lhighgui

HEADERS  += mainwindow.h \
    webcameracv.h \
    loadtempimage.h

SOURCES += main.cpp mainwindow.cpp \
    webcameracv.cpp \
    loadtempimage.cpp