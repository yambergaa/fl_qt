﻿#include "mainwindow.h"
#include <QApplication>
#include <QTextCodec>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    MainWindow *mainWindow = new MainWindow(argv);

    mainWindow->show();

    int r = app.exec();
    delete mainWindow; //так деструкторы дочерних виджетов отрабатываются

    return r;
}
